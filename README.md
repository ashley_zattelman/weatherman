This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Launch instructions
### 1) `npm i`
### 2) `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Supported Browsers
It is recommended to open the app in Google Chrome and enable location services


## Environment
Node v6.11.3
import React, { Component } from 'react';
import jQuery               from 'jquery';
import WeatherIcon          from 'react-icons-weather';
import MaterialIcon, {colorPallet} from 'material-icons-react';
import sun                  from './assets/sun.svg';
import overcast             from './assets/overcast.svg';
import cloudy               from './assets/cloudy.svg';
import cry                  from './assets/cry.svg';
import wind                 from './assets/wind.svg';
import rain                 from './assets/rain.svg';
import thunder              from './assets/thunder.svg';


class App extends Component {

    currentLocation() {
        this._geoFindLocation();
    }

    refreshData() {
        this.setState({load: true});
        this._fetchWeather();
    }

    render() {
    let forecast,
        appContent,
        backgroundClass     = "";

    //Set form structure to and render after API load
    let appForm             = (
            <div className="weather__form">
                <form onSubmit={this._handleSubmit.bind(this)} className="weather__form__form">
                    <input autoComplete="off" className="weather__form__input"    type="text" id="place" name="place" placeholder="City"/>
                    <button className="weather__form__button weather__form__button--submit"   type="submit">
                        <MaterialIcon icon="search" invert />
                    </button>
                </form>

                <button className="weather__form__button" onClick={(e) => this.currentLocation(e)}><MaterialIcon icon="location_searching" invert /></button>
                <button className="weather__form__button" onClick={(e) => this.refreshData(e)}><MaterialIcon icon="refresh" invert /></button>
            </div>
        );

        //Set app content if weather results returned
        if (this.state.weather ) {
            forecast        = this._buildForecast();
            backgroundClass = generateWeatherClass(this.state.weather.item.condition.text);

            appContent      = (
                <div className="weather">
                    { appForm }
                    <WeatherLogo text={this.state.weather.item.condition.text}/>
                    <WeatherHeader
                        location={this.state.weather.location.city}
                        temp={this.state.weather.item.condition.temp}
                        text={this.state.weather.item.condition.text}
                        unit={this.state.weather.units.temperature}/>

                    <div className="overflow-scroll">
                        <div className="forecast">{forecast}</div>
                    </div>
                </div>
            );

        }
        //Return Error state
        else {
            appContent = (
                <div className="weather__error">
                    {appForm}
                    <img src={ cry } className="weather__error__icon" alt="logo" />
                    <p className="weather__error__message">{this.state.message}</p>
                </div>
            );
        }


        return <div className={ "app" + ' app--' + backgroundClass }>
            <div className="header">
                <MaterialIcon icon="accessibility" invert />
                <span className="header__title">WEATHERMAN</span>
            </div>
            <div className="app__gradient-overlay"></div>
            { this.state.load ? (<img src={ sun } className="weather__loader" alt="logo" />) : (appContent) }
        </div>;
    }

    constructor() {
        super();
        this.state = {
            load: false,
            search: '',
            message: '',
            latitude: null,
            longitude: null,
            weather: {
                units: {
                    temperature: 'C'
                },
                location: {
                    city: 'Loading'
                },
                item: {
                    title: '',
                    condition: {
                        temp: 0,
                        text: ''
                    },
                    forecast: []
                }
            }
        };
    }

    //Fetch geoLocation on load
    componentDidMount() {
        this._geoFindLocation();
    }

    //Weather search - submit handler
    _handleSubmit(e) {
        e.preventDefault();
        let v = document.getElementById('place').value;

        if (v.length) {
            this.setState({search: v, load: true}, function () {
                this._fetchWeather();
            });
        }
    }

    //Compile 10 day forecast
    _buildForecast() {
        return this.state.weather.item.forecast.map(d => {
            return (<WeatherForecast key={d.date} day={d.day} high={d.high} low={d.low} text={d.text} code={d.code} date={d.date}/>);
        });
    }

    //Yahoo Service call
    _fetchWeather(latlong) {
        let latLongQuery    = `select * from weather.forecast where u='c' and woeid in (SELECT woeid FROM geo.places WHERE text="(${this.state.latitude}, ${this.state.longitude})")`,
            searchQuery     = `select * from weather.forecast where u='c' and woeid in (select woeid from geo.places(1) where text="${this.state.search}")`,
            errorMessage    = 'Sorry, weather in this location is unavailable';

        let query = latlong ?  latLongQuery : searchQuery;

        jQuery.ajax({
            url: 'https://query.yahooapis.com/v1/public/yql',
            type: 'GET',
            data: {
                q: query,
                format: 'json',
                env: 'store://datatables.org/alltableswithkeys'
            },
            success: (res) => {
                if (res.query.results) {
                    this.setState({search: res.query.results.channel.location.city, weather: res.query.results.channel});
                } else {
                    this.setState({weather: null, message: errorMessage});
                }

                this.setState({load: false});
            },
            error: (error) => {
                this.setState({weather: null, message: errorMessage, load: false});
            }
        });
    }

    //Retrieve users current location using HTML5 - navigator.geolocation
    _geoFindLocation() {
        if (!navigator.geolocation){
            return;
        }

        let success = (position) => {
            var latitude  = position.coords.latitude;
            var longitude = position.coords.longitude;

            this.setState({ latitude: latitude, longitude: longitude}, function () {
                this._fetchWeather(true);
            });
        };

        let error = () => {
            let errorMessage = "Doh! Looks like your browser doesn't support geo-location. Try searching a particular city or open the app in either Chrome or Firefox.";
            this.setState({load: false, weather: null, message: errorMessage});
        };

        this.setState({load: true});
        navigator.geolocation.getCurrentPosition(success, error);
    }

}

//return custom logo dependant on todays' weather
class WeatherLogo extends Component {
    render() {
        let map = {
            "Mostly Cloudy":            cloudy,
            "Partly Cloudy":            overcast,
            "Cloudy":                   overcast,
            "Showers":                  rain,
            "Rain":                     rain,
            "Scattered Thunderstorms":  thunder,
            "Sunny":                    sun,
            "Breezy":                   wind,
        };

        let logo = map[this.props.text];

        if (!logo) {
            logo = sun;
        }

        return <img src={logo} className="weather__logo" alt="logo" />
    }
}

//format weather text into workable css class
function generateWeatherClass(weather) {
    let n               = weather.split(" "),
        backgroundClass = "";

    for (let i = 0; i < n.length; i++) {
        backgroundClass += n[i].toLowerCase();
        if(i < n.length - 1) {
            backgroundClass += '-';
        }
    }

    return backgroundClass;
}


//Todays' weather section
class WeatherHeader extends Component {
    render() {
        return <div className="weather__header">
            <div className="weather__header__location">{this.props.location}</div>
            <div className="weather__header__text">{this.props.text}</div>
            <div className="weather__header__temp">{this.props.temp}&deg;</div>
        </div>;
    }

}

//multi day forecast component
class WeatherForecast extends Component {
    render() {
        const icon = `wi wi-yahoo-${this.props.code}`;

        return <div className="forecast__item">
            <div className="forecast__item__row">
                <div className="forecast__item__day">{this.dayExpander(this.props.day)}</div>
            </div>
            <div className="forecast__item__row">
                <div className="forecast__item__icon"><i className={ icon }></i></div>
                <div className="forecast__item__temp">
                    <div className="forecast__item__temp__high">{this.props.high}</div>
                    <div className="forecast__item__temp__low">{this.props.low}</div>
                </div>
            </div>
        </div>
    }


    dayExpander(day) {
        let week = {
            'Mon': 'Monday',
            'Tue': 'Tuesday',
            'Wed': 'Wednesday',
            'Thu': 'Thursday',
            'Fri': 'Friday',
            'Sat': 'Saturday',
            'Sun': 'Sunday'
        };
        return week[day];
    }
}

export default App;
